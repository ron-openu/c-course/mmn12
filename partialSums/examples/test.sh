#! /bin/bash

PROG=partialSums

for input in *.input ; do
  echo ~~~~~~~~~~~~~~~ $input ~~~~~~~~~~~~~~~
  output="${input%.input}.output"
  ../$PROG < $input > $output
  cat $output
done
