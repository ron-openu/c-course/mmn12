/******************************************************************************
 * File:    partialSums.c
 * Author:  Ron Hasson
 * Purpose: the program reads an array of numbers and its size and then 
 *          prints the array of partial sums
 ******************************************************************************/

/******************************************************************************
 *** INCLUDES
 ******************************************************************************/
#include <stdio.h>
#include <stdlib.h>

/******************************************************************************
 *** MACROS
 ******************************************************************************/

/******************************************************************************
 *** INTERNAL FUNCTIONS DECLARATIONS
 ******************************************************************************/
int *partialSums(int *array, unsigned int size);

/******************************************************************************
 *** EXTERNAL FUNCTIONS DEFINITIONS
 ******************************************************************************/

/******************************************************************************
 * Func:    main
 * Purpose: see program's purpose above
 * Params:  None
 * Returns: 0 on success, 1 on memory allocation errors
 ******************************************************************************/
int main(void){
    int i = 0;
    unsigned int input_size = 0;
    int *input_array = 0;
    int *returned_array = 0;

    /* read input */
    printf("enter the size of the array: ");
    scanf("%u", &input_size);
    input_array = malloc(sizeof(int) * input_size);
    /* validating memory allocation success */
    if (NULL == input_array) {
        fprintf(stderr, "Error occurred while trying to allocate memory");
        return 1;
    }
    printf("enter %d items one after another: ", input_size);
    for (i = 0; i < input_size; i++){
        scanf("%d", input_array+i);
    }
    printf("\n");

    /* pretty print input */
    printf("Inputs:\n\tArray size: %d\n\tItems: ",input_size);
    for (i = 0; i < input_size; i++){
        printf("%d", input_array[i]);
        if(i<input_size-1){
             printf(", ");
        }
    }
    printf("\n\n");

    returned_array = partialSums(input_array, input_size);
    /* validating memory allocation success */
    if (NULL == returned_array) {
        fprintf(stderr, "Error occurred while trying to allocate memory");
        return 1;
    }

    /* printing the returned array of partialSums */
    printf("partial sums array: \n\t");
    for (i = 0; i < input_size; i++){
        printf("%d ", returned_array[i]);
        if(i<input_size-1){
             printf(", ");
        }
    }
    printf("\n");

    /* free all allocated memory */
    free(input_array);
    free(returned_array);

    /* exit program */
    return 0;
}

/******************************************************************************
 * Func:    partialSums
 * Purpose: gets and array of ints and its size, and then returns a new array
 *          of partial sums (sum until the items's index)
 * Params:  array - array of integers. the function will not mutate it.
            size - length of the array
 * Returns: new array of partial sums, which needs to be freed in the caller 
            function.
 ******************************************************************************/
int *partialSums(int *array, unsigned int size){
    int i = 0;
    /* used to save the summed value up until the index */
    int middleSum = 0;
    int *sumsArr = NULL;
    
    sumsArr = malloc(sizeof(int) * size);
    /* validating memory allocation success */
    if (NULL == sumsArr) {
        fprintf(stderr, "Error occurred while trying to allocate memory");
        return NULL;
    }

    /* update the summed value('middleSum') and saving it in the array for each index */
    for(i = 0; i<size; i++){
        middleSum += array[i];
        sumsArr[i] = middleSum;
    }

    return sumsArr;
}
